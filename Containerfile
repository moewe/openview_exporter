# https://playwright.dev/python/docs/docker
# Based on https://github.com/microsoft/playwright-python/blob/main/utils/docker/Dockerfile.noble

# https://mcr.microsoft.com/en-us/product/playwright/python/about
FROM mcr.microsoft.com/playwright/python:v1.50.0 as builder
HEALTHCHECK CMD curl --fail http://localhost:9876

WORKDIR /app
RUN chown ubuntu:ubuntu /app

USER ubuntu

# https://hynek.me/articles/docker-uv/
ENV UV_LINK_MODE=copy \
    UV_COMPILE_BYTECODE=1 \
    UV_PYTHON_DOWNLOADS=never

COPY --from=ghcr.io/astral-sh/uv:latest /uv /usr/local/bin/uv
COPY README.md uv.lock pyproject.toml openview_exporter.py ./

RUN uv sync \
  --locked \
  --no-dev \
  --no-install-project

# Try to use unbuffered output to see logs in container,
# see https://stackoverflow.com/q/29663459
# but it doesn't work still
ENTRYPOINT ["uv", "run", "./openview_exporter.py"]
