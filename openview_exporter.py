#!/usr/bin/env python3
"""Export prometheus metrics from openview."""

import logging
import os
import re
import time

from playwright.sync_api import Playwright, TimeoutError, sync_playwright  # noqa: A004
from playwright.sync_api._generated import Page
from prometheus_client import (
    GC_COLLECTOR,
    PLATFORM_COLLECTOR,
    PROCESS_COLLECTOR,
    REGISTRY,
    Gauge,
    start_http_server,
)

logger = logging.getLogger(__name__)


def run(playwright: Playwright) -> None:  # noqa: PLR0915
    """Run main routine."""

    def extract_number(field: str) -> float:
        """Extract a number from a string."""
        nr_str: str = re.findall(r"\d+[,.\d]*", field)[0]
        nr = nr_str.replace(",", ".")
        return float(nr)

    def set_metric(  # noqa: PLR0913
        page: Page,
        prometheus_metric: Gauge,
        label: str,
        locator: str,
        exact: bool = False,  # noqa: FBT001, FBT002
        nth: int = 0,
    ) -> None:
        """Scrapes a field and sets a given prometheus metric.

        If the locator contains "xpath", the field is located by page.locator(),
        otherwise by page.get_by_text().
        Defaults to the first one found (nth=0).
        """
        metric_name = str(prometheus_metric.describe()[0]).split(",")[0].split("(")[1]  # pyright: ignore[reportIndexIssue,reportUnknownArgumentType]
        metric_desc = str(prometheus_metric.describe()[0]).split(",")[1]  # pyright: ignore[reportIndexIssue,reportUnknownArgumentType]
        try:
            if re.compile(r"xpath=|id:").search(str(locator)):
                logger.debug("Locating by page.locator(): %s", locator)
                html = page.locator(locator).nth(nth).inner_html()
            else:
                logger.debug("Locating by page.textfield(): %s", locator)
                html = str(
                    page.get_by_text(locator, exact=exact).nth(nth).text_content()
                )
            logger.debug("Got html: %s", html)
            value = extract_number(html)
        except TimeoutError:
            logger.warning(
                "!!! Playwright timeout error trying to parse %s (%s) !!!",
                metric_name,
                metric_desc,
            )
            openview_failures.labels(metric_name=metric_name).inc()
            value = 0
        logger.debug("  %s (%s): %s", metric_name, metric_desc, value)
        prometheus_metric.labels(sensor=label).set(value)

    def navigate(page: Page, text: str, nth: int = 0) -> None:
        """Click on the n-th navigation item matching a given text.

        Defaults to the first one found (nth=0).
        """
        logger.debug("Navigating to %s (nth=%s)", text, nth)
        page.get_by_text(text).nth(nth).click()
        time.sleep(wait_for_javascript)

    ##############
    # Main routine
    ##############

    # Mandatory env vars
    ov_password = os.environ["OV_PASSWORD"]
    http_username = os.environ["HTTP_USERNAME"]
    http_password = os.environ["HTTP_PASSWORD"]

    # Optional env vars
    headless = os.getenv("HEADLESS", "True").lower() in ("true", "1", "t")
    debug = os.getenv("DEBUG", "False").lower() in ("true", "1", "t")
    # Seconds to wait before looping again (default: 20 min)
    interval = int(os.getenv("INTERVAL", str(20 * 60)))

    loglevel = logging.DEBUG if debug else logging.INFO
    logging.basicConfig(level=loglevel)

    port = 9876

    # Seconds to wait for javascript to fill in the fields
    # 1 sec is too short
    wait_for_javascript = 2

    # Start browser context
    browser = playwright.chromium.launch(headless=headless)
    context = browser.new_context(
        http_credentials={"username": http_username, "password": http_password}
    )
    page = context.new_page()

    # Initialize Prometheus instrumentation
    # https://github.com/prometheus/client_python
    #
    # Disable default boilerplate matrics
    REGISTRY.unregister(GC_COLLECTOR)
    REGISTRY.unregister(PLATFORM_COLLECTOR)
    REGISTRY.unregister(PROCESS_COLLECTOR)

    # Initialize metrics

    openview_failures = Gauge(
        "openview_failures", "Failures fetching metrics", ["metric_name"]
    )

    # Ereignisse
    openview_meldung = Gauge("openview_meldung", "Meldungen", ["kategorie", "meldung"])

    # Anlage1
    openview_temp = Gauge("openview_temp", "Temperatur (°C)", ["sensor"])
    openview_betriebstunden = Gauge(
        "openview_betriebstunden", "Betriebsstunden (h)", ["sensor"]
    )
    openview_wrg_klappe = Gauge("openview_wrg_klappe", "WRG Klappe (%)", ["sensor"])
    openview_druck = Gauge("openview_druck", "Druck (Pa)", ["sensor"])
    openview_druck_anforderung = Gauge(
        "openview_druck_anforderung", "Druck Anforderung (%)", ["sensor"]
    )
    openview_festsollwert = Gauge(
        "openview_festsollwert", "Frequenzumformer Festsollw. (%)", ["sensor"]
    )
    openview_feuchtigkeit = Gauge(
        "openview_feuchtigkeit", "Luftfeuchtigkeit (%H)", ["sensor"]
    )
    openview_taupunkt = Gauge("openview_taupunkt", "Taupunkt", ["sensor"])

    # Differenz Regler Drehzahl/Temperatur
    openview_lueftungsleistung = Gauge(
        "openview_lueftungsleistung", "Lüftungsleistung (%)", ["sensor"]
    )

    _ = start_http_server(port)
    logger.info("debug mode: %s", debug)
    logger.info("Starting http server on port %s", port)

    # TODO : Wo genau befinden sich Fühler 1 + 2
    # https://openproject.moewe-altonah.de/projects/it/work_packages/1518/
    temp_fuehler = {
        "Keller1": "Heizungsraum",
        "Keller2": "Keller2",
        "Keller3": "Keller3",
    }

    try:
        while True:
            # Session timeout is 3mins no matter what happens, śo re-login is needed on
            # each loop
            openview_failures.clear()

            logger.debug("Logging in")
            _ = page.goto("https://lueftung.moewe.link/client/index.html")
            page.get_by_text("Kennwort", exact=True).click()
            page.get_by_role("button", name="login").click()
            page.get_by_role("textbox").fill(ov_password)
            page.get_by_role("button", name="übernehmen").click()
            time.sleep(wait_for_javascript)

            ############
            # Ereignisse
            ############

            navigate(page, "Ereignisse")
            meldungen = page.get_by_text(re.compile("(Meldung|Störung)"))

            openview_meldung.clear()
            kategorie = "unknown"
            count = meldungen.count()
            logger.warning("%i Meldungen !", count)
            for meldung in meldungen.all():
                # Get element text and remove leading "Meldung "
                text = meldung.all_inner_texts()[0].split(" ", 1)[1]
                logger.debug("Meldung: %s", text)
                if re.search(r"Wartung am ..-Ventilator", text):
                    kategorie = "wartung_ventilator"
                elif re.search(r"AU-Filter Lüftungsanlage verschmutzt", text):
                    kategorie = "filter_verstopft"
                elif re.search(r"^BSK.*", text):
                    kategorie = "brandschutzklappe"

                openview_meldung.labels(kategorie=kategorie, meldung=text).inc()

            ##########
            # Anlage 1
            ##########
            # xpath_prefix for Anlage1
            xpath_prefix = (
                "xpath=/html/body/div[2]/div[2]/div/div[3]"
                "/div/div[4]/div/div[2]/div/div/div/"
            )
            navigate(page, "Anlage1")

            ## Temperatur Regler Anlage 1
            ### Soll-und Istwerte
            navigate(page, "Temperatur Regler Anlage")
            navigate(page, "Soll-und Istwerte")

            set_metric(page, openview_temp, "Zuluft_sollwert_aktiv", "ZU-Sollw.aktiv")

            #### ab-temp (Need to use div locator since the value is a single field)
            set_metric(page, openview_temp, "Abluft", xpath_prefix + "div[9]")
            set_metric(page, openview_temp, "Zuluft", "ZU-Temperatur")
            set_metric(page, openview_temp, "Aussen", "AU-Temperatur")
            # Abluft, die vor dem Wärmetauscher wieder zurück nach draussen geführt wird
            set_metric(
                page, openview_temp, "Fortluft_vor_Wärmetauscher", "FO-Temperatur"
            )

            #### WRG-Klappe (Need to use div locator since the value is a single field)
            set_metric(page, openview_wrg_klappe, "", xpath_prefix + "div[16]")

            # Close open Navigation point to prevent
            # double entries like "Soll-und-Istwerte"
            navigate(page, "Temperatur Regler Anlage 1")

            ## Druckregler Zuluft
            navigate(page, "Druckregler Zuluft")
            navigate(page, "Soll-und Istwerte")
            set_metric(page, openview_druck, "Zuluft_soll", "Sollwert aktiv")
            set_metric(page, openview_druck, "Zuluft_ist", "Druck-Istwert")
            set_metric(page, openview_druck_anforderung, "Zuluft", "Anforderung")
            navigate(page, "Druckregler Zuluft")

            ## Druckregler Abluft
            navigate(page, "Druckregler Abluft")
            navigate(page, "Soll-und Istwerte")
            set_metric(page, openview_druck, "Abluft_soll", "Sollwert aktiv")
            set_metric(page, openview_druck, "Abluft_ist", "Druck-Istwert")
            set_metric(page, openview_druck_anforderung, "Abluft", "Anforderung")
            navigate(page, "Druckregler Abluft")

            ## ZU-Ventilator Anlage 1
            ### Betriebsstundenzähler
            navigate(page, "ZU-Ventilator Anlage 1")
            navigate(page, "Betriebsstundenzähler")

            set_metric(page, openview_betriebstunden, "Abgelaufen", "abgelaufen")
            set_metric(
                page,
                openview_betriebstunden,
                "Wartungsintervall",
                "Intervall",
            )

            ## Frequenzumformer Zuluft
            navigate(page, "Frequenzumformer Zuluft")
            navigate(page, "Ventilator Festsollw. Zuluft")
            set_metric(page, openview_festsollwert, "Zuluft_aktuell", "aktueller FSW")
            ## Frequenzumformer Abluft
            navigate(page, "Frequenzumformer Abluft")
            navigate(page, "Ventilator Festsollw. Abluft")
            set_metric(page, openview_festsollwert, "Abluft_aktuell", "aktueller FSW")

            ##############
            # Mieterkeller
            ##############
            navigate(page, "Mieterkeller")

            ## Differenz Regler Drehzahl/Temperatur
            navigate(page, "Differenz Regler Drehzahl/Temperatur")
            navigate(page, "Soll- und Istwerte")
            set_metric(page, openview_lueftungsleistung, "", xpath_prefix + "div[4]")

            ## Feuchte Regler
            # "RA-Sollwert Feuchte r.F.": Sollwert der Mieterkeller Feuchtigkeit
            navigate(page, "Feuchte Regler", 0)
            navigate(page, "Soll-und Istwerte")
            set_metric(
                page,
                openview_feuchtigkeit,
                "Aussen",
                "Feuchte r.F.",
                exact=False,
                nth=1,
            )
            set_metric(
                page, openview_taupunkt, "Aussen", "Taupunkt", exact=False, nth=2
            )

            ## MIN-/MAX-/Mittelwert Feuchtigkeit
            navigate(page, "MIN-/MAX-/Mittelwert", 0)
            ### Raumfühler 1 (Need to use div locator since the value is a single field)
            set_metric(
                page,
                openview_feuchtigkeit,
                temp_fuehler["Keller1"],
                xpath_prefix + "div[3]",
            )
            ### Raumfühler 2 (Need to use div locator since the value is a single field)
            set_metric(
                page,
                openview_feuchtigkeit,
                temp_fuehler["Keller2"],
                xpath_prefix + "div[6]",
            )
            ### Raumfühler 3 (Need to use div locator since the value is a single field)
            set_metric(
                page,
                openview_feuchtigkeit,
                temp_fuehler["Keller3"],
                xpath_prefix + "div[9]",
            )
            ### Raumfühler Mittelwert
            set_metric(page, openview_feuchtigkeit, "Keller_avg", "akt.Wert")

            ## MIN-/MAX-/Mittelwert Temperatur
            navigate(page, "MIN-/MAX-/Mittelwert", 1)
            ### Raumfühler 1 (Need to use div locator since the value is a single field)
            set_metric(
                page, openview_temp, temp_fuehler["Keller1"], xpath_prefix + "div[3]"
            )
            ### Raumfühler 2 (Need to use div locator since the value is a single field)
            set_metric(
                page, openview_temp, temp_fuehler["Keller2"], xpath_prefix + "div[6]"
            )
            ### Raumfühler 3 (Need to use div locator since the value is a single field)
            set_metric(
                page, openview_temp, temp_fuehler["Keller3"], xpath_prefix + "div[9]"
            )
            ### Raumfühler Mittelwert
            set_metric(page, openview_temp, "Keller_avg", "akt.Wert")

            if debug:
                logger.debug(
                    "\nFinished collecting metrics, sleeping %i seconds.\n", interval
                )
            time.sleep(int(interval))
    except KeyboardInterrupt:
        context.close()
        browser.close()


with sync_playwright() as playwr:
    run(playwr)
