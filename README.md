# Openview Prometeus exporter

- Prometheus exporter for [OpenView](https://www.deos-controls.com/programmable-controllers/openview/) metrics
- _Beware_: Work in progress !
- Scrapes the web inferface using [Playwright](https://playwright.dev/)
- Tested with OpenView version `2.3.8, Build 51`

## Setup

Export the OpenView password and HTTP basic auth credentials:

```sh
export OV_PASSWORD=$(gopass show --password mnt/moewe/iot/lueftung/passwort-ebene/2-unprivilegiert)
export HTTP_USERNAME=moewe
export HTTP_PASSWORD=$(gopass show --password mnt/moewe/www/lueftung.moewe.link/moewe)
```

If you want to view the browser interaction:

```sh
export HEADLESS=false
```

### Authentication

Default passwords:

- `1-unprivilegiert-RO` (Privilege level `1`): Cannot read `Betriebstunden abgelaufen`
  Fa. Steininger unfortunately cannot change this
- `2-unprivilegiert` (Privilege level `1`): Can read all neccessary metrics, and should be used
- `E-privilegiert` (Privilege level `E`): Can read all neccessary metrics, but should not be
  used for monitoring

### Usage

### Docker image

```sh
task run:docker
curl -Ssl localhost:9876
```

### Local setup

Install [uv](https://docs.astral.sh/uv/getting-started/installation/)
Download playwright Chromium browser:

```sh
task install:browser
```

Run the exporter:

```sh
task run
```

## Development

Start codegen:

```sh
playwright codegen
```

Enter URL and http basic auth credentials
Browse

## To-do

- Reduce container image size (1 GB with uv, 0.8 GB before)
